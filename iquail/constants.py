
class Constants:
    IQUAIL_ROOT_NAME = ".iquail"
    ARGUMENT_UNINSTALL = "--iquail_uninstall"
    ARGUMENT_BUILD = "--iquail_build"
    ARGUMENT_RM = "--iquail_rm"
    CHECKSUMS_FILE = ".integrity.json"
    INTEGRITY_IGNORE_FILE = ".integrity_ignore"
    VERSION_FILE = "solution_version.txt"
    CONFIG_FILE = "config.ini"
    IQUAIL_LAUNCHER_NAME = "iquail_launcher"
